<?php
/**
 * @file
 * syclops_connections.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function syclops_connections_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation:node/add/ssh-config
  $menu_links['navigation:node/add/ssh-config'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/ssh-config',
    'router_path' => 'node/add/ssh-config',
    'link_title' => 'Connection',
    'options' => array(
      'attributes' => array(
        'title' => 'Configuration params required to connect to remote server machine .',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Connection');


  return $menu_links;
}
