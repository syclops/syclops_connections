<?php
/**
 * @file
 * syclops_connections.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function syclops_connections_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_connection_settings|node|ssh_config|form';
  $field_group->group_name = 'group_connection_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ssh_config';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Connection Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_syclops_config_max_session',
      1 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Connection Settings',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-connection-settings field-group-fieldset col-md-6 pull-right',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_connection_settings|node|ssh_config|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_remote_system_details|node|ssh_config|form';
  $field_group->group_name = 'group_remote_system_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ssh_config';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Remote System Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_ssh_config_machine',
      1 => 'field_ssh_config_machine_user',
      2 => 'field_ssh_config_private_key',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Remote System Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-remote-system-details field-group-fieldset col-md-6 pull-left',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_remote_system_details|node|ssh_config|form'] = $field_group;

  return $export;
}
