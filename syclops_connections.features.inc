<?php
/**
 * @file
 * syclops_connections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function syclops_connections_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function syclops_connections_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function syclops_connections_node_info() {
  $items = array(
    'ssh_config' => array(
      'name' => t('Connection'),
      'base' => 'node_content',
      'description' => t('Configuration params required to connect to remote server machine .'),
      'has_title' => '1',
      'title_label' => t('Connection Alias'),
      'help' => t('Enter an alias or unique name for this connection for easy reference. By default, Syclops uses the standard SSH syntax of <username>@<hostname>. You can change this to any name of your choice, such as "Server 1 Root", or "Dev Server 2 for Project X".'),
    ),
  );
  return $items;
}
