<?php
/**
 * @file
 * syclops_connections.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function syclops_connections_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'member-ssh-config';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'ssh_config' => 'ssh_config',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_activities-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_activities-block_3',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-syclops_connection-block_1' => array(
          'module' => 'views',
          'delta' => 'syclops_connection-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-syclops_connection-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_connection-block_3',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['member-ssh-config'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'project-admin-ssh-config';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'ssh_config' => 'ssh_config',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_activities-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_activities-block_3',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-syclops_connection-block_1' => array(
          'module' => 'views',
          'delta' => 'syclops_connection-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-syclops_connection-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_connection-block_3',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['project-admin-ssh-config'] = $context;

  return $export;
}
