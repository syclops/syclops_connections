<?php
/**
 * @file
 * syclops_connections.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function syclops_connections_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Active Sessions';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'projects_active_sessions';
  $fe_block_boxes->body = '<?php
 global $user;
 $session =  syclops_ui_get_projects_active_session_details();
if(isset($session)) {
 $close_button = \'\';
 foreach($session as $active_session) {

  if($user->uid == $active_session[\'user_id\']) {
    $close_button = \'<a href="/session/\'. $active_session[\'session_id\']. \'/forceclose"><span class="btn btn-xs btn-default btn-danger">close</span></a>\';
  }
  print \'<h4><span class="label label-success">\'.$active_session[\'connection_name\'].\'</span><span> \'.$close_button.\'</span></h4>\';
 print \'<small><span class="glyphicon glyphicon-time"></span> Started \'.$active_session[\'session_start_time\'].\' by <span class="glyphicon glyphicon-user"></span> \'. $active_session[\'user_name\'].\'
</small><br>\';
 print \'<hr>\';
}
} 
?>';

  $export['projects_active_sessions'] = $fe_block_boxes;

  return $export;
}
