<?php
/**
 * @file
 * syclops_connections.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function syclops_connections_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-projects_active_sessions'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'projects_active_sessions',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'user/*',
    'roles' => array(
      'authenticated user' => '2',
    ),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => 'Active Sessions',
    'visibility' => '0',
  );

  $export['views-syclops_connection-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_connection-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_connection-block_3'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_connection-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_connection-block_7'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_connection-block_7',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_connection-block_8'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_connection-block_8',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_connection-block_9'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_connection-block_9',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_5'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_5',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_6'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}
